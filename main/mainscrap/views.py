from . import mainscrap
from .. import models
from bs4 import BeautifulSoup as bs
from main import db
from flask import render_template
import requests as req


def request(url):
    return req.get(url).text


def sabun_cantik(data):
    return bs(data, "lxml")


def simpan_array(data):
    array = []
    for d in data:
        array.append(d)
    return array


def proccess(type):
    if type == "News" or type == "Sport":
        url = 'https://news.detik.com/mostpopular?mpnews'
        a = request(url)
        soup = sabun_cantik(a)
        data = soup.find('div', {'class': 'lf_content boxlr w870 indeks_mostpop'})
        data2 = data.find_all('div', {'class': 'desc_idx fr w500'})
        return data2

    elif type == "Tekno":
        url = 'https://inet.detik.com/mostpopular?mpinet'
        a = request(url)
        soup = sabun_cantik(a)
        data = soup.find('div', {'class': 'box'})
        data2 = data.find_all('ul', {'class': 'list feed'})
        # print(data2)
        return data2

    elif type == "SepakBola":
        url = 'https://sport.detik.com/sepakbola/mostpopular?mpsepakbola'
        a = request(url)
        soup = sabun_cantik(a)
        data = soup.find('ul', {'class': 'list feed'})
        # print(data)
        data2 = data.find_all('li')
        return data2

    elif type == "Hot":
        url = 'https://hot.detik.com/mostpopular?mphot'
        a = request(url)
        soup = sabun_cantik(a)
        data = soup.find('div', {'class': 'lf_content boxlr w870 indeks_mostpop'})
        data2 = data.find_all('div', {'class': 'list feed'})
        return data2

    elif type == "Finance":
        url = 'https://finance.detik.com/mostpopular?mpfinance'
        a = request(url)
        soup = sabun_cantik(a)
        data = soup.find('ul', {'class': 'list feed'})
        data2 = data.find_all('div', {'class': 'desc_idx fr w500'})
        return data2

    elif type == "Oto":
        url = 'https://oto.detik.com/mostpopular?mpoto'
        a = request(url)
        soup = sabun_cantik(a)
        data = soup.find('div', {'class': 'lf_content fl w870'})
        data2 = data.find_all('div', {'class': 'list feed'})
        return data2

    elif type == "Health":
        url = 'https://health.detik.com/mostpopular?mphealth'
        a = request(url)
        soup = sabun_cantik(a)
        data = soup.find('div', {'class': 'box'})
        data2 = data.find_all('div', {'class': 'feed_grid two_row mt10'})
        return data2

    elif type == "Travel":
        url = 'https://travel.detik.com/mostpopular?mptravel'
        a = request(url)
        soup = sabun_cantik(a)
        data = soup.find('section', {'class': 'list__news'})
        data2 = data.find_all('article', {'class': 'list__news--trigger  clearfix'})
        return data2

    elif type == "Food":
        url = 'https://food.detik.com/mostpopular?mpfood'
        a = request(url)
        soup = sabun_cantik(a)
        data = soup.find('div', {'class': 'box'})
        data2 = data.find_all('ul', {'class': 'list feed'})
        # print(data2)
        return data2

    elif type == "Wolipop":
        url = 'https://wolipop.detik.com/mostpopular?mpwolipop'
        a = request(url)
        soup = sabun_cantik(a)
        data = soup.find('ul', {'class': 'list_feed'})
        data2 = data.find_all('li')
        return data2

    elif type == "TV":
        url = 'https://20.detik.com/?mp'
        a = request(url)
        soup = sabun_cantik(a)
        data = soup.find('div', {'class': 'box pd15 mb30'})
        print(data)
        data2 = data.find_all('article')
        return data2

    return None


def get_data_scrap(data, type):
    if type == "News" or type == "Sport":
        urls = data.a['href']
        juduls = data.h2.text
        dat = data.find('span', {'class': 'labdate f11 mt5 mb5'}).text
        teasers = data.find('span', {'class': 'labdate f11 mt5 mb5'}).next_sibling
        return query_sql(urls, juduls, dat, teasers, type)

    elif type == "Tekno" or type == "Oto" or type == "Travel" or type == "Food":
        urls = data.a['href']
        juduls = data.h2.text
        dat = data.find('div', {'class': 'box_text'}).find('div',{'class':'date'}).text
        teasers = data.find('div', {'class': 'box_text'}).find('div',{'class':'date'}).next_sibling
        return query_sql(urls, juduls, dat, teasers, type)

    elif type == "Health":
        urls = data.a['href']
        juduls = data.h2.text
        dat = data.find('div', {'class': 'box_text'}).find('span',{'class':'date'}).text
        # print(dat)
        teasers = data.find('div', {'class': 'box_text'}).find('span',{'class':'date'}).next_sibling
        return query_sql(urls, juduls, dat, teasers, type)

    elif type == "SepakBola":
        urls = data.a['href']
        juduls = data.h2.text
        dat = data.find('div', {'class': 'desc_idx fr w500'}).find('span',{'class':'labdate f11 mt5 mb5'}).text
        teasers = data.find('div', {'class': 'desc_idx fr w500'}).find('span',{'class':'labdate f11 mt5 mb5'}).next_sibling
        return query_sql(urls, juduls, dat, teasers, type)

    elif type == "Hot":
        urls = data.a['href']
        juduls = data.h2.text
        dat = data.find('div', {'class': 'desc_idx fr w500'}).find('div',{'class':'labdate f11 mt5 mb5'}).text
        teasers = data.find('div', {'class': 'desc_idx fr w500'}).find('div',{'class':'labdate f11 mt5 mb5'}).next_sibling
        return query_sql(urls, juduls, dat, teasers, type)

    elif type == "Finance":
        urls = data.a['href']
        juduls = data.h2.text
        dat = data.find('span',{'class':'labdate f11 mt5 mb5'}).text
        teasers = data.find('span',{'class':'labdate f11 mt5 mb5'}).next_sibling
        return query_sql(urls, juduls, dat, teasers, type)

    elif type == "Wolipop":
        urls = data.a['href']
        juduls = data.h3.text
        dat = data.find('span', {'class': 'time'}).text
        teasers = " Kosong "
        return query_sql(urls, juduls, dat, teasers, type)

    elif type == "TV":
        urls = data.a['href']
        juduls = data.h3.text
        dat = " Kosong "
        teasers = " Kosong "
        return query_sql(urls, juduls, dat, teasers, type)


def query_sql(url, judul, time, teaser, category):
    submit = models.NewsScraptNews(
        title_news=judul,
        url_news=url,
        teaser=teaser,
        tanggal=time,
        category=category
    )
    return submit

def save(data):
    if data is not None:
        db.session.add(data)
        db.session.commit()
        return True
    return False

def scrapt_news(types):
    multiple = []
    data2 = proccess(types)
    for container in data2:
        if container.find_all('li') is not None:
            data = get_data_scrap(container, types)
            multiple.append(data)
    for mul in multiple:
        db.session.add(mul)
        db.session.commit()
    return True

    # return data.find_all('li')


def get_data(type):
    return scrapt_news(types = type)


@mainscrap.route('/')
def mainapp():
    type = ["News", "Sport", "Tekno", "Finance", "Oto", "Health", "Travel", "Food", "Wolipop"]
    data = None
    for ty in type:
        print(ty)
        if get_data(ty) is not None:
            data = models.NewsScraptNews.query.all()
        else:
            return "Error"
    return render_template('text.html', data=data)
    # return "Koneksi Mati!"

@mainscrap.route('/detail_news')
def news_detail():
    news = models.NewsScraptNews.query.with_entities(models.NewsScraptNews.url_news, models.NewsScraptNews.title_news).all()[0]
    return get_detailed(news)

def get_detailed(dat):
    data = request(dat.url_news)
    sabun = sabun_cantik(data)
    data = sabun.find('article')
    isi_news = data.find('div',{'class':'itp_bodycontent detail_text'}).text
    comment = sabun.find('div',{'class':'komentar_box'}).find('div',{'id':'thecomment2'}).find('div',{'class':'xcomponent xcomponent-tag-thecomment2 xcomponent-context-iframe'})

    print(sabun)
    return isi_news
