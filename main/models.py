from main import db

class NewsScraptNews(db.Model):
    __tablename__ = 'news_newscrapt'

    id = db.Column(db.Integer, primary_key=True)
    title_news = db.Column(db.String(255), nullable=False)
    url_news = db.Column(db.Text)
    teaser = db.Column(db.Text)
    tanggal = db.Column(db.String(255),nullable=False)
    category = db.Column(db.String(255), nullable=False)

    def __str__(self):
        return '{%s}' % self.title_news

    def __repr__(self):
        return '{%s}' % self.title_news

class NewsDetailScrapDetik(db.Model):
    __tablename__ = 'news_detailed_news'

    id = db.Column(db.Integer, primary_key=True)
    title_news = db.Column(db.Text)
    isi_news = db.Column(db.Text)
    comment = db.Column(db.Integer, nullable=True)

    def __str__(self):
        return '{%s}' % self.id_news

    def __repr__(self):
        return '{%s}' % self.id_news